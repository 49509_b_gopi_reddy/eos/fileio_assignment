#include<stdio.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include <stdlib.h>

typedef enum gender{
    MALE=1,FEMALE
}gender_t;
 struct address{
       char city [20];
       char state[10];
   };
typedef struct filesys
{
int id; 
char name[50]; 
int age ;
gender_t gen;
char mobile[10];
char email[20];
struct address addr;
}filysys_t;
void accept_address(struct address *addr){
        printf("city:   ");
        scanf("%s",addr->city);
        printf("state:  ");
        scanf("%s",addr->state);
}
void print_address(struct address *addr){
    printf("city:   %s \n",addr->city);
    printf("state:  %s \n",addr->state);
}
void accept_record(filysys_t *ptr){
    printf("id:   ");
    scanf("%d",&ptr->id);
    printf("name:   ");
    scanf("%s",ptr->name);
    printf("age:    ");
    scanf("%d",&ptr->age);
    printf("Gender gen:(1-MALE/2-FEMALE):    ");
    scanf("%d",(int*)&(ptr->gen));
    printf("mobile:   ");
    scanf("%s",ptr->mobile);
    printf("email:   ");
    scanf("%s",ptr->email);  
    accept_address(&ptr->addr);
       
}
void print_record(filysys_t *ptr){
    printf("%d      %s      %d      %s      %s      %s      %s      %s\n",ptr->id,ptr->name,ptr->age,ptr->gen ==1 ? "MALE" : "FEMALE",ptr->mobile,ptr->email,(char*)&ptr->addr.city,(char*)&ptr->addr.state);
}
void add_adresses(const char *path){
int fp = open(path,O_WRONLY | O_APPEND | O_CREAT , 0777 );
    if(fp!=-1){
        filysys_t addre;
        accept_record(&addre);
        write(fp,&addre,sizeof(filysys_t));
        close(fp);
    }else
    perror("failed to write in path");  
}
void Display(const char *path){
int fp= open (path, O_RDONLY);
    if(fp!=-1){
       filysys_t addre;
       while(read(fp,&addre,sizeof(filysys_t))>0)
       print_record(&addre);
       close(fp);
    }else
        perror("failed to read in path");
}
void Person_Address(const char *path, int id){
int fp= open (path, O_RDONLY);
    if(fp!=-1){
       filysys_t addre;  
        while(read(fp,&addre,sizeof(filysys_t))>0)
            if(addre.id==id)
                print_record(&addre);
        close(fp);
    } else
         perror("failed to read in path");
}
void Edit_Address_Person(const char *path ,int id){
    int fp= open (path, O_RDWR);
    if(fp!=-1){
       filysys_t addre;
        while(read(fp,&addre,sizeof(filysys_t))>0)
            if(addre.id==id){
                printf("new city:   ");
                scanf("%s",addre.addr.city);
                printf("new state:  ");
                scanf("%s",addre.addr.state);
                lseek(fp,-1*sizeof(filysys_t),SEEK_CUR);
                write(fp,&addre,sizeof(filysys_t));
            }
            else
            perror("failed to edit in path");
    }
}
void Delete_Address(const char *path){
    int fp= open (path, O_RDONLY);
    int fd= open("temp.dat",O_WRONLY|O_APPEND|O_CREAT,0777);
    if(fp!=-1){
        int id;
        printf("id:   ");
        scanf("%d",&id);
       filysys_t addre;
        while(read(fp,&addre,sizeof(filysys_t))>0){
        if(addre.id!=id)
        write(fd,&addre,sizeof(filysys_t));
        }
        close(fd);
        close(fp);
        remove(path);
        rename("temp.dat",path);
    }else
    perror("failed to edit in path");
}
void City_address(const char *path){
    int fp= open (path, O_RDONLY);
      if(fp!=-1){
          char city[20];
        printf("city:   ");
        scanf("%s",city);
        filysys_t addre;
        while(read(fp,&addre,sizeof(filysys_t))>0){
           // lseek(fp,0*sizeof(filysys_t),SEEK_CUR);
            if(strcmp(addre.addr.city,city)==0)
           //printf("addre.city: %s\n",addre.addr.city);
             print_record(&addre);
             
        }
     
        close(fp);
      }else
      perror("failed to display_city_address in path");
}
/*int count_age(){
    int count=0;
    int fp= open ("file.dat", O_RDONLY);
         if(fp!=-1){
             filysys_t addre;
              while(read(fp,&addre,sizeof(filysys_t))>0)
              count++;
              printf("value: %d\n",count);
         }
              close(fp);
}
void Sort_Addresses(const char *path){
    
}*/
int menu(void){
    int select;
        printf("0.exit\n");
        printf("1.add_adresses\n");
        printf("2.Display_All_Addresses\n");
        printf("3.Display_Address_of_a_Person\n");
        printf("4.Edit_Address_of_a_Person\n");
        printf("5.Delete_Address_of_a_Person\n");
        printf("6.Display_All_Addresses_from_a_City\n"); 
        printf("7.Sort_Addresses_by_age_of_a_Person\n");
        printf("select a number:        ");
        scanf("%d",&select);
        return select;
}
int main(void){
    int select;
   /* int count;
            count=count_age();
    filysys_t *arr;
        arr=(filysys_t*)malloc(count*sizeof(filysys_t)); 

    */

    //sort_add(arr,count);
    //print_arr(arr,count);
    while((select=menu())!=0){
        switch(select){
            case 1:
                 add_adresses("file.dat");
                break;
          case 2:
                Display("file.dat");
                break;
              case 3:
                Person_Address("file.dat",1);
                break;
           case 4:
                Edit_Address_Person("file.dat",1);
                break;
            case 5:
                Delete_Address("file.dat");
                break;
            case 6:
                City_address("file.dat");
                break;
           /* case 7:
                Sort_Addresses("file.dat");
                break;    
                */
        }
    }
    return 0;
}